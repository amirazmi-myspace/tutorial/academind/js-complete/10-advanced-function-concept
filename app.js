// PURE FUNCTIONS - Produce exact expected value & no side effects to outside code
function add(num1, num2) {
    return num1 + num2;
}
// console.log(add(1, 2));
// console.log(add(5, 8));

// IMPURE FUNCTIONS - Return unexpected value & has side effects to outside code
function addRandom(num1) {
    return num1 + Math.random()
}
// console.log(addRandom(34))

let previousResult = 0;
function addMoreNumbers(num1, num2) {
    const sum = num1 + num2;
    previousResult = sum;
    return sum;
}
// console.log(addMoreNumbers(5, 4));

const hobbies = ['Sports', 'Cooking'];
function printHobbies(h) {
    const copied = [...hobbies]
    copied.push('NEW HOBBY');
    console.log(copied);
}
// printHobbies(hobbies);
// console.log(hobbies);




// FACTORY FUNCTIONS
function createTaxCalculator(tax) {
    function calculateTax(amount) {
        return amount * tax;
    }
    return calculateTax;
}

// const vatAmt = calculateTax(100, 0.19);
// const incomeTax = calculateTax(100, 0.25);

const calculateVatAmount = createTaxCalculator(0.19);
const calculateIncomeTaxAmount = createTaxCalculator(0.25);
// console.log(calculateVatAmount(100))
// console.log(calculateIncomeTaxAmount(100))




// CLOSURES - Functions remember the surrounding variables
let userName = 'Max'
function greetUser() {
    let name = 'Anna';
    console.log('Hi ' + name);
}

let name = 'Maxi'
userName = 'Manuel';
// greetUser();





// RECURSION
// function powerOf(x, n) {
//     let result = 1;
//     for (let i = 0; i < n; i++){
//         result *= x;
//     }
//     return result;
// }

function powerOf(x, n) {
    if (n === 1) return x;
    return x * powerOf(x, n - 1);
}
// console.log(powerOf(2, 3))

const myself = {
    name: 'Max',
    friends: [
        {
            name: 'Manuel',
            friends: [
                { name: 'Chris' }
            ]
        },
        { name: 'Julia' }
    ]
};

function printFriendName(person) {
    const collectedName = [];
    if (!person.friends) return [];

    for (const friend of person.friends) {
        collectedName.push(friend.name);
        collectedName.push(...printFriendName(friend));
    }
    return collectedName;
}
console.log(printFriendName(myself));